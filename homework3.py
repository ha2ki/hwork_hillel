#Homework3
"""
Завдання 1.Зформуйте строку, яка містить певну інформацію про символ по його індексу в відомому слові.
Наприклад "The [індекс символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
Слово та номер отримайте за допомогою input() або скористайтеся константою.
Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".
"""
print('Завдання 1.')
print('Введіть, будь ласка, слово: ')
word = input()
while True:
    userinput_number = input('Введіть, будь ласка, айді символу(цифрафи): ')

    if userinput_number.isdigit():
        userinput_number = int(userinput_number)
        break

    print(f'{userinput_number} - не може бути такого айді, спробуйте ще раз!')
print('Айді --->', userinput_number)
if userinput_number <= len(word):
    symbol = word[userinput_number]
    print(f'Індекс символу - {userinput_number} у слові {word} це символ {symbol}')
else:
    print('У вашому слові не має такого id(занадто велике значення)')

print('')

"""
Завдання 2. Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою). 
Напишіть код, який визначить кількість слів, в цих даних.
"""
print('Завдання 2.')
print('Введіть, будь ласка, строку для визначення к-ті слів: ')
my_str = input()
counter = len(my_str.split())
print(f'Кількість слів: {counter} ')

print('')

"""
Завдання 3. Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. 
Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1. 
Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.
"""
print('Завдання 3')
lst1 = ['99', True, 131, "Homework", 'print', 5.5, '6', 18, 4, 'Python', 9.0, 717, 'list']
print(lst1)
lst2 = [x for x in lst1 if not isinstance(x, (int,float))]
print(lst2)
