print('''
／ﾌﾌ 　　　　　 　　 　ム｀ヽ
/ ノ)　　 ∧　　∧　　　　）　ヽ
/ ｜　　(´・ω ・`）ノ⌒（ゝ._,ノ
/　ﾉ⌒＿⌒ゝーく　 ＼　　／
丶＿ ノ 　　 ノ､　　|　/
　　 `ヽ `ー-‘人`ーﾉ /
　　　 丶 ￣ _人’彡ﾉ
　　　／｀ヽ _/\__'
''')


#task1
"""
Створіть дві змінні first=10, second=30.
Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.
"""
print('task1: ')
first = 10
second = 30

result = first + second
print(result)

result = first - second
print(result)

result = first * second
print(result)

result = first / second
print(result)

result = first ** second
print(result)

result = first // second
print(result)

result = first % second  # 10 / 30  -> 10 - 0 * 30
print(result)

print('  ')


#task2
"""
 Створіть змінну і запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
  Виведіть на екран результат кожного порівняння.
"""
print('task2: ')

my_bool = True
my_bool = False

my_bool = first < second
print(my_bool)

my_bool = first > second
print(my_bool)

my_bool = first == second
print(my_bool)

my_bool = first != second
print(my_bool)

print('  ')


#task3
"""
 Створіть змінну - результат конкатенації (складання) строк str1="Hello " и str2="world".
  Виведіть на екран.
"""
print('task3: ')
str1='Hello '
str2='world'
print(str1+str2)


